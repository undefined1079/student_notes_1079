'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require("cors")
const bcrypt = require('bcrypt')
const initializePassport = require("./passport-config")
const passport = require("passport")
const flash = require("express-flash")
const session = require("express-session")

initializePassport(passport, async email => {
    let user = await User.findOne({
        where: {
            email: email,
        }})
        return user;
}, async id => {
    let user = await User.findOne({
        where: {
            id: id,
        }})
        return user;
})

const sequelize = new Sequelize('notesApp_db', 'app', 'welcome123', {
    dialect : 'mysql'
})

const User = sequelize.define('user', {
    id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true 
    },
    firstName : Sequelize.STRING,
    lastName : Sequelize.STRING,
    classNr : Sequelize.STRING,
    faculty : Sequelize.STRING,
    email : Sequelize.STRING,
    password : Sequelize.STRING
})

const Note = sequelize.define('note', {
    id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true 
    },
    title : Sequelize.STRING,
    content : Sequelize.STRING,
    tag : Sequelize.STRING,
    subject : Sequelize.STRING,
    data : Sequelize.DATE
},
{
    timestamps : false
})

const UserNote = sequelize.define('usernote', {})

const Group = sequelize.define('group',{
    id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true 
    },
    title: Sequelize.STRING
})

const GroupSubscription = sequelize.define('groupsubscription',{})

GroupSubscription.belongsTo(Group)
GroupSubscription.belongsTo(User)
Group.hasMany(GroupSubscription)
User.hasMany(GroupSubscription)

const GroupNote = sequelize.define('groupnote',{})
GroupNote.belongsTo(Group, {
    onDelete: 'cascade',
    hooks: true,
})
GroupNote.belongsTo(Note)
Group.hasMany(GroupNote)
Note.hasMany(GroupNote)


UserNote.belongsTo(Note,{ 
  onDelete: 'cascade',
  hooks: true, 
})
UserNote.belongsTo(User)
User.hasMany(UserNote)
Note.hasMany(UserNote)

const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use(flash())
app.use(session({
    secret: "ourSecret",
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())


app.get('/', async (req, res, next) =>{});

app.post('/register', async (req, res, next) =>{
     try {
        
         let user = req.body 
            user.email = req.body.email
            user.password = await bcrypt.hash(req.body.password, 10)
            user.firstName = req.body.firstName
            user.lastName = req.body.lastName
            user.faculty = req.body.faculty
            user.classNr = req.body.classNr
            await User.create(user)
            res.status(200).json({message: 'created'})
        
    }catch(err){
        next(err)
    }
});

app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { 
        res.status(500).json({message: 'error'}) 
    }
    if (!user) { 
        res.status(404).json({message: 'no user found'}) 
    }
    req.logIn(user, function(err) {
      if (err) { 
          return next(err); 
      }
    res.status(200).json(user)
       
    });
  })(req, res, next);
});

app.post('/usernote', async(req, res, next) => {
    try{
        let usernote = req.body
        usernote.noteId= req.body.noteId
        usernote.userId=req.body.userId
        
        await UserNote.create(usernote)
        
        res.status(200).json({message:'created'})
    }catch(err){
        next(err)
    }
})

app.post('/user/:userId/group', async(req, res, next) => {
    try{
        let group = req.body
        group.title= req.body.title
        let myGroup = await Group.create(group)
        await GroupSubscription.create(
            {
            userId: req.params.userId,
            groupId: myGroup.id
            })
        res.status(200).json({message:'created'})
    }catch(err){
        next(err)
    }
})

app.get('/user/:userId/groups', async(req, res, next) => {
     try {
        let group = await Group.findAll({
           include : [ {
               model : GroupSubscription,
               where : { userId : req.params.userId}
           }]
        })
        if (group) {
            res.status(200).json(group)
        } else {
          res.status(404).json({message : 'not found'})  
        }
    } catch (err) {
        next(err)
    }
})

app.get('/groupsubscriptions', async(req, res, next) =>{
   try {
        let groups = await GroupSubscription.findAll()
        res.status(200).json(groups)
    } catch (err) {
       next(err)
    }  
})

app.post('/groupsubscription/:uid/:gid', async(req, res, next) => {
    try{
        let groupsubscription = req.params
        groupsubscription.userId= req.params.uid
        groupsubscription.groupId=req.params.gid
        
        await GroupSubscription.create(groupsubscription)
        
        res.status(200).json({message:'created'})
    }catch(err){
        next(err)
    }
})

app.get('/groupnotes', async(req, res, next) =>{
   try {
        let groups = await GroupNote.findAll()
        res.status(200).json(groups)
    } catch (err) {
       next(err)
    }  
})

app.get('/group/:id/notes', async(req, res, next) =>{
   try {
        let groupnotes = await Note.findAll({
           include : [ {
               model : GroupNote,
                   where : { groupId : req.params.id}
           }]
        })
        if (groupnotes) {
            res.status(200).json(groupnotes)
        } else {
          res.status(404).json({message : 'not found'})  
        }
    } catch (err) {
        next(err)
    } 
})

app.delete('/user/:id/groups/:gid', async(req, res, next) => {
    try{
            let groupSubscription = await GroupSubscription.findAll({
                where:{
                    userId: req.params.id,
                    groupId : req.params.gid
                }
            })
            if(groupSubscription){
                let subscription = groupSubscription.shift()
                if(subscription){
                    await subscription.destroy()
                     res.status(200).json({message: 'not found'})
                }
            }
        else{
            res.status(404).json({message: 'not found'})
        }
    }catch(err){
        next(err)
    }
})

app.get('/groups', async (req, res, next) =>{
    try {
        let groups = await Group.findAll()
        res.status(200).json(groups)
    } catch (err) {
       next(err)
    }
})

app.post('/groupnote/:groupId/:noteId', async(req, res, next) => {
    try{
        let groupnote = req.params
        groupnote.groupId= req.params.groupId
        groupnote.noteId=req.params.noteId
        
        await GroupNote.create(groupnote)
        
        res.status(200).json({message:'created'})
    }catch(err){
        next(err)
    }
})

app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (e) {
       next(e)
    }
})

app.get('/users/:id/notes', async (req, res, next) => {
    try {
        let user = await Note.findAll({
           include : [ {
               model : UserNote,
                   where : { userId : req.params.id}
           }]
        })
        if (user) {
            res.status(200).json(user)
        } else {
          res.status(404).json({message : 'not found'})  
        }
    } catch (err) {
        next(err)
    }
})

app.get('/users/:id/notes/:tag', async (req, res, next) => {
    try {
        let user = await Note.findAll({
            where: {
                tag: sequelize.where(sequelize.fn('LOWER', sequelize.col('tag')), 'LIKE', '%' + req.params.tag + '%')
            },
           include : [ {
               model : UserNote,
                   where : { userId : req.params.id}
           }]
        })
        if (user) {
            res.status(200).json(user)
        } else {
          res.status(404).json({message : 'not found'})  
        }
    } catch (err) {
        next(err)
    }
})

app.post('/users/:id/notes', async (req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id, {
            include : [UserNote] 
        })
        if(user){
            
         let note = req.body 
            note.data = Date.now();
         let nots = await Note.create(note)
            UserNote.create({
                userId:user.id,
                noteId:nots.id
            })
            
            res.status(200).json({message: 'created'})
        }else{
            res.status(404).json({message: 'not found'})
        }
    }catch(err){
        next(err)
    }
});

// app.post('/users', async (req, res, next) => {
//     try {
//          let user = req.body 
//             user.email = req.body.email
//             user.password = req.body.password
//             user.firstName = req.body.firstName
//             user.lastName = req.body.lastName
//             user.faculty = req.body.faculty
//             user.classNr = req.body.classNr
//             await User.create(user)
            
//             res.status(200).json({message: 'created'})
        
//     }catch(err){
//         next(err)
//     }
// });

app.put('/users/:id/notes/:nid', async(req, res, next) => {
    try{
        let notes = await Note.findAll({
               include : [{
                   model : UserNote,
                       where : { userId : req.params.id,
                           noteId : req.params.nid
                       }
                       }]
                })
            let note = notes.shift()
            if(note){
                await note.update(req.body, {
                    fields : ['content', 'title','tag','subject']
                })
                res.status(200).json({message: "accepted"})
            }
        
        else{
            res.status(404).json({message: 'not found'})
        }
    }catch(err){
        next(err)
    }
})

app.get('/users/:id/notes/:nid', async(req, res, next) => {
    try{
            let note = await Note.findAll({
               include : [{
                   model : UserNote,
                       where : { userId : req.params.id,
                           noteId : req.params.nid
                       }
                       }]
                })
                if (note) {
                    res.status(200).json(note)
                }else{ 
                 res.status(404).json()
                }
    }catch(err){
        next(err)
    }
})

app.delete('/users/:id/notes/:nid', async(req, res, next) => {
    try{
        let notes = await Note.findAll({
              include : [{
                  model : UserNote,
                      where : { userId : req.params.id,
                          noteId : req.params.nid
                      }
                      }]
                })
        if(notes){
            let usernotes = await UserNote.findAll({
                where:{
                    userId: req.params.id,
                    noteId : req.params.nid
                }
            })
            if(usernotes){
                let usernote = usernotes.shift()
                if(usernote){
                    await usernote.destroy()
                     res.status(200).json({message: 'not found'})
                }
            }
        }
        
        else{
            res.status(404).json({message: 'not found'})
        }
    }catch(err){
        next(err)
    }
})

app.get('/users', async (req, res, next) =>{
    try {
        let users = await User.findAll()
        res.status(200).json(users)
    } catch (err) {
       next(err)
    }
})

app.get('/usernote', async (req, res, next) =>{
    try {
        let usernote = await UserNote.findAll()
        res.status(200).json(usernote)
    } catch (err) {
       next(err)
    }
})

app.put('/users/:id', async (req,res, next) => {
    try {
     let user = await User.findByPk(req.params.id)
     if (user) {
         await user.update(req.body)
         res.status(202).json({message : 'accepted'})
     } else {
       res.status(404).json({message : 'not found'})  
     }
 } catch (err) {
     next(err)
 } 
})

app.delete('/users/:id', async (req,res, next) => {
 try {
     let user = await User.findByPk(req.params.id)
     if (user) {
         await user.destroy()
         res.status(202).json({message : 'accepted'})
     } else {
       res.status(404).json({message : 'not found'})  
     }
 } catch (err) {
     next(err)
 } 
})

app.use((err, req ,res, next) => {
 console.warn(err)
 res.status(500).json({message : '>:('})
})


app.listen(8080)